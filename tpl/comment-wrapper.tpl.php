<?php

/**
 * @file comment-wrapper.tpl.php
 */
?>
<div id="comments">
  <h3><?php echo t('Comments');?></h3>
  <?php print $content; ?>
</div>
