<?php

/**
 * @file
 * City Theme
 * by Template-Stock.com
 * v.2010
 * http://template-stock.com/
 * 
 */

/**
 * Initialize theme settings.
 */
if (is_null(theme_get_setting('city_swf_header'))) {
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(
    'city_swf_header' => 1,
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

/**
 * Implementation of theme_node_submitted.
 */
function city_node_submitted($node) {
  return t('By !username : @datetime',
    array(
      '@datetime' => format_date($node->created, 'custom', 'd M Y H:i'),
      '!username' =>theme('username', $node)
    ));
 }
 
/**
 * Implementation of theme_comment_submitted.
 */
function city_comment_submitted($comment) {
  return t('By !username : @datetime',
    array(
      '!username' => theme('username', $comment),
      '@datetime' => format_date($comment->timestamp, 'custom', 'd M Y H:i')
    ));
}

/**
 * Implementation of theme_feed_icon().
 */
function city_feed_icon($url, $title) {
  return '';
}

/**
 * Implementation of theme_preprocess_page().
 */
function city_preprocess_page(&$variables) {
  $header_set = theme_get_setting('city_swf_header');
  $swf = base_path() . drupal_get_path('theme', 'city') .'/swf/city.swf';
  $is_myref = strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']);

  if (($is_myref > 0) && ($header_set == 1)) {
    $swf .= '?animation=0';
  }

  $header = '
    <object
      classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
      codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0"
      id="Gorod02"
      width="980" height="286"
    >
      <param name="movie" value="'. $swf .'">
      <param name="bgcolor" value="#FFFFFF">
      <param name="quality" value="high">
      <param name="wmode" value="transparent">
      <param name="seamlesstabbing" value="false">
      <param name="allowscriptaccess" value="samedomain">
      <embed
        type="application/x-shockwave-flash"
        pluginspage="http://www.adobe.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
        name="City"
        width="980" height="286"
        src="'. $swf .'"
        bgcolor="#FFFFFF"
        quality="high"
        wmode="transparent"
        seamlesstabbing="false"
        allowscriptaccess="samedomain"
      >
        <noembed>
        </noembed>
      </embed>
    </object>';

  $variables['swfheader'] = $header;
}

function city_preprocess_comment(&$variables) {
  $comment = $variables['comment'];
  $node = $variables['node'];

  $variables['title'] = l('#', $_GET['q'], array('fragment' => "comment-$comment->cid"));

}