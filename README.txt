
-- SUMMARY --

City Drupal Theme.

For a full description of the theme, visit the project page:
  http://drupal.org/project/city
OR
  http://template-stock.com/free-drupal-theme/city.html

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/city


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/getting-started/install-contrib/themes
  for further information.


-- CONFIGURATION --

* Theme Configure in Administer >> Site building >> Themes >> Configure >>
  City.


-- CONTACT --

Current maintainers:
* Paul Stocker (Stocker) - http://drupal.org/user/809274

This project has been sponsored by:
* Template-Stock.com
  Visit http://template-stock.com/ for more information.

Welcome all feedback:
* http://template-stock.com/contact
