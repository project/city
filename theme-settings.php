<?php
/*
 * City Theme Settings
 */

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function city_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
    'city_swf_header' => 1,
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['city_swf_header'] = array(
    '#type' => 'select',
    '#title' => t('Display Mode of flash header'),
    '#options' => array(
      0 => t('Always animation'),
      1 => t('Animation once'),
    ),
    '#default_value' => $settings['city_swf_header'],
  );

  // Return the additional form widgets
  return $form;
}